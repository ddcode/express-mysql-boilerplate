import * as jwt from "jsonwebtoken"

const loggedIn = (req, res, next) => {
    let token = req.header ? req.header('Authorization') : false;
    if (!token) return res.status(401).send("Access Denied");

    try {
        if (token.startsWith('Bearer ')) {
            // Remove Bearer from string
            token = token.slice(7, token.length).trimLeft();
        }
        const verified = jwt.verify(token, process.env.APP_SESSION_SECRET);
        if (verified.user_role === 3) {
            let req_url = req.baseUrl + req.route.path;
            if (req_url.includes("users/:id") && parseInt(req.params.id) !== verified.id) {
                return res.status(401).send("Unauthorized!");
            }
        }
        req.user = verified;
        next();
    } catch (err) {
        res.status(400).send("Invalid Token");
    }
}

const open = async (req, res, next) => {
    next()
}

const adminOnly = async (req, res, next) => {
    let token = req.header ? req.header('Authorization') : false;
    if (!token) return res.status(401).send("Access Denied");

    try {
        if (token.startsWith('Bearer ')) {
            // Remove Bearer from string
            token = token.slice(7, token.length).trimLeft();
        }

        const verified = jwt.verify(token, process.env.APP_SESSION_SECRET);
        if (![1,2].includes(req.user.user_role)) {
            return res.status(401).send("Unauthorized!");
        }

        req.user = verified;
        next();
    } catch (err) {
        res.status(400).send("Invalid Token");
    }
}

export {adminOnly, loggedIn, open}