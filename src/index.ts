import "reflect-metadata";
import {createConnection} from "typeorm";
import * as cors from "cors"
import Routes from "./routes";
import * as dotenv from 'dotenv'
import * as express from "express";
import * as bodyParser from "body-parser";
import {Request, Response} from "express";
import {allowHeaders, allowMethods} from './config/allow'


import {networkInterfaces} from "os";
const nets = networkInterfaces();
const results = Object.create(null); // Or just '{}', an empty object

// for (const name of Object.keys(nets)) {
//     for (const net of nets[name]) {
//         // Skip over non-IPv4 and internal (i.e. 127.0.0.1) addresses
//         if (net.family === 'IPv4' && !net.internal) {
//             if (!results[name]) {
//                 results[name] = [];
//             }
//             results[name].push(net.address);
//         }
//     }
// }
//
// const IPAddress = results["en0"][0]

import {adminOnly, loggedIn, open} from './helpers/auth'

dotenv.config()

// Telegram
const wakeupNotification = false

const host = process.env.APP_HOST

createConnection().then(async connection => {
    // create express app
    const app = express();
    app.use(bodyParser.json());

    app.use(cors({
        // origin: (origin, callback) => {
        //     // allow requests with no origin
        //     // (like mobile apps or curl requests)
        //     if (!origin) return callback(null, true);
        //
        //     if (allowOrigin.includes(origin)) {
        //         const msg = 'The CORS policy for this site does not ' +
        //             'allow access from the specified Origin.';
        //         return callback(new Error(msg), false);
        //     }
        //
        //     return callback(null, true);
        // }

        origin: "*"
    }))

    // Headers config
    app.use(function (req, res, next) {
        res.header('Access-Control-Allow-Methods', allowMethods.join(','))
        res.header('Access-Control-Allow-Headers', allowHeaders.join(','))
        next()
    })

    // register express routes from defined application routes
    Routes.forEach(route => {
        // @ts-ignore
        (app as any)[route.method](route.route, route.auth === 'user' ? loggedIn : route.auth === 'admin' ? adminOnly : open, (req: Request, res: Response, next: Function) => {
            const result = (new (route.controller as any))[route.action](req, res, next);
            if (result instanceof Promise) {
                result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);

            } else if (result !== null && result !== undefined) {
                res.json(result);
            }
        });
    });

    // start express server
    app.set('port', process.env.APP_PORT || 9999)
    app.listen(app.get('port'), host);

    console.log(`Express server has started on port http://localhost:${app.get('port')}.`);
}).catch(error => console.log(error));
