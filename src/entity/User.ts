import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany} from "typeorm";
import {Role} from "./Role";

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    ID: number;

    @Column("varchar", {
        length: 60,
        nullable: false,
    })
    name: string;

    @Column("varchar", {
        length: 60,
        nullable: false,
        unique: true
    })
    email: string;

    @Column("varchar", {
        length: 60,
        nullable: false,
    })
    password: string;

    @ManyToOne(type => Role, role => role.ID, {
        nullable: false
    })
    role: Role;

    @Column("boolean")
    is_active: boolean;

    @Column("timestamp")
    updated: Date;

    @Column({
        type: "timestamp",
        default: () => 'CURRENT_TIMESTAMP'
    })
    created: Date;
}
