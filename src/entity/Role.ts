import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany} from "typeorm";

@Entity()
export class Role {
    @PrimaryGeneratedColumn()
    ID: number;

    @Column("varchar", {
        length: 24,
        nullable: false,
    })
    name: string;

    @Column("varchar", {
        length: 24,
        nullable: false,
    })
    slug: string;
}
