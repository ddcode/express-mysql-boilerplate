export const allowOrigin = [
    // 'http://localhost:3000',
    '*'
]
export const allowMethods = [
    "GET", "HEAD", "OPTIONS", "POST", "PUT", "DELETE", "PATCH"
]
export const allowHeaders = [
    "Origin", "X-Requested-With", "Content-Type", "Accept", "Authorization"
]
