export const RoleSeed = [
    {
        name: `Super administrator`,
        slug: `superadmin`
    },
    {
        name: `Administrator`,
        slug: `admin`
    },
    {
        name: `User`,
        slug: `user`
    },
];