import {UserController} from "./controller/UserController";
import {loggedIn, adminOnly} from "./helpers/auth"

let routes = [] as any

const defaultCRUD = [
    {
        name: "users",
        controller: UserController,
        auth: "user"
    },
]

defaultCRUD.map(item => {
    const record = [
        {
            method: "get",
            route: `/${item.name}`,
            controller: item.controller,
            auth: item.auth,
            action: "all"
        },
        {
            method: "get",
            route: `/${item.name}/:id`,
            controller: item.controller,
            auth: item.auth,
            action: "one"
        },
        {
            method: "post",
            route: `/${item.name}`,
            controller: item.controller,
            auth: item.auth,
            action: "save"
        },
        {
            method: "delete",
            route: `/${item.name}/:id`,
            controller: item.controller,
            auth: item.auth,
            action: "remove"
        }
    ]

    routes = [...routes, ...record]
});


const authenticateRoutes = [
    {
        method: "post",
        route: `/user/login`,
        controller: UserController,
        action: "login",
        auth: 'open'
    },
    {
        method: "post",
        route: `/user/register`,
        controller: UserController,
        action: "register",
        auth: 'adminOnly'
    },
]

routes = [...routes, ...authenticateRoutes]

export default routes