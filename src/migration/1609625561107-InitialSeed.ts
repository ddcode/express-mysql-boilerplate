import {getRepository, MigrationInterface, QueryRunner} from "typeorm";
import {Role} from "../entity/Role";
import {RoleSeed} from "../seeds/role.seed";

export class ExampleSeed1609625561107 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await getRepository(Role).save(RoleSeed)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }
}
