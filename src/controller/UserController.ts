import * as bcrypt from "bcrypt"
import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {User} from "../entity/User";
import * as jwt from "jsonwebtoken"

export class UserController {
    private repo = getRepository(User);

    async all(request: Request, response: Response, next: NextFunction) {
        return this.repo.find({relations: ['role']});
    }

    async one(request: Request, response: Response, next: NextFunction) {
        return this.repo.findOne(request.params.id);
    }

    async save(request: Request, response: Response, next: NextFunction) {
        return this.repo.save(request.body);
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        const idToRemove = await this.repo.findOne(request.params.id);
        await this.repo.remove(idToRemove);
    }

    async login(request: Request, response: Response, next: NextFunction) {
        try {
            // Check user exist
            const user = await this.repo.findOne({
                email: request.body.email,
            }, {
                relations: ['role']
            });

            if (user && user.is_active) {
                const validPass = await bcrypt.compare(request.body.password, user.password);
                if (!validPass) return response.status(400).send("Email or password is wrong");

                delete user.password

                // Create and assign token
                const token = jwt.sign({ID: user.ID, user_role: user.role}, process.env.APP_SESSION_SECRET);
                return response.status(200).header("auth-token", token).send({token, user});
            } else {
                return response.status(400).send(`Something went wrong`)
            }
        } catch (err) {
            const error_data = {
                entity: 'User',
                model_obj: {param: request.params, body: request.body},
                error_obj: err,
                error_msg: err.message
            };

            return response.status(500).send("Error retrieving User");
        }

    }

    async register(request: Request, response: Response, next: NextFunction) {
        const salt = await bcrypt.genSalt(10);
        const hasPassword = await bcrypt.hash(request.body.password, salt);

        const data = {
            email: request.body.email,
            name: request.body.name,
            role: {
                ID: request.body.roleID
            },
            password: hasPassword,
            is_active: true,
        };

        try {
            const user = await this.repo.save(data)
            delete user.password
            return user;
        } catch (err) {
            return response.status(500).send(err);
        }
    }
}