# Express + MySQL with TypeORM

Steps to run this project:

1. Run `npm i` command
2. Setup database settings inside `ormconfig.json` file
3. Run `npm start` command

## How to seed

If you want to prefill data with custom records run `npm run seed`
